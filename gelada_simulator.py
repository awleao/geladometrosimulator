import sys
import time
import random
import socket
import thread

# # # # # # # # # # # # # # # # # # # # 
# All rigths reserveds to Brains++    #
# # # # # # # # # # # # # # # # # # # #

def progress_step(thread_name, idx):
	j = 1
	while True:
		j = j + 1
		if (j % 4) == 0:
			sys.stdout.write("\r    \r")
			sys.stdout.flush()
		else:
			sys.stdout.write(".")
			sys.stdout.flush()
		time.sleep(0.2)


print "[Geladometro Simulator] Setting-up"

HOST = '192.168.0.113' #'107.170.100.214'     # Endereco IP do Servidor
PORT = 9999                  # Porta que o Servidor esta
tcp  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
dest = (HOST, PORT)

print "[Geladometro Simulator] Connecting to the server"

tcp.connect(dest)

print "[Geladometro Simulator] Conected"

CERVA = "004"                 # MARCA DA CERVEJA
BAR   = "002"                 # ID do BAR

print "[Geladometro Simulator] Sending temperature data"

try:
	thread.start_new_thread( progress_step, (" ", 0) )
	while True:
		TEMP = random.randrange(-10, 40, 1)
		STR  = CERVA + "," + str(TEMP) + "," + BAR + "," + "0" 
		tcp.send(STR)
		time.sleep(5)
except (KeyboardInterrupt, SystemExit):
	print "[Geladometro Simulator] Interrupting"
except Exception as e:
	print e

print "[Geladometro Simulator] Shuting down"
tcp.close()